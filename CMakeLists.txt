
######
# check the CMake configuration
######

# check if cmake has the required version
cmake_minimum_required(VERSION 2.8.0 FATAL_ERROR)

# set the verbosity
set(CMAKE_VERBOSE_MAKEFILE 0)  # if set to 1 compile and link commands are displayed during build
option(DEBUG_OUTPUT "en/disable debug output" OFF)

# define the project
message(STATUS "")
message(STATUS ">>> Setting up project 'Tracking_HandsOn_HighRR'.")
project(Tracking_HandsOn_HighRR)

# load some common cmake macros
# set path, where to look first for cmake modules, before ${CMAKE_ROOT}/Modules/ is checked
message(STATUS "")
message(STATUS ">>> Setting up Cmake modules.")
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmakeModules")
message(STATUS "Using cmake module path '${CMAKE_MODULE_PATH}'.")
include(CommonMacros)
include(FeatureSummary)

# force out-of-source builds
enforce_out_of_source_build()


######
# check for the system configuration
######

message(STATUS "")
message(STATUS ">>> Setting up the system environment")

#’warn’ the user if system is not UNIX
if(NOT UNIX)
        message(FATAL_ERROR "This is an unsupported system.")
endif()

#check for the system configuration
if(CMAKE_VERSION VERSION_GREATER 2.8.4)
        include(ProcessorCount)  # requires Cmake V2.8.5+
        ProcessorCount(NMB_CPU_CORES)
        message(STATUS "Detected host system '${CMAKE_HOST_SYSTEM_NAME}' version "
                "'${CMAKE_HOST_SYSTEM_VERSION}', architecture '${CMAKE_HOST_SYSTEM_PROCESSOR}', "
                "${NMB_CPU_CORES} CPU core(s).")
else()
        message(STATUS "Detected host system '${CMAKE_HOST_SYSTEM_NAME}' version "
                "'${CMAKE_HOST_SYSTEM_VERSION}', architecture '${CMAKE_HOST_SYSTEM_PROCESSOR}'.")
endif()

message(STATUS "Compiling for system '${CMAKE_SYSTEM_NAME}' version '${CMAKE_SYSTEM_VERSION}', "
        "architecture '${CMAKE_SYSTEM_PROCESSOR}'.")

      
######
# set the compiler
######

set(CMAKE_CXX_FLAGS "-std=c++11 -fPIC -Wall -Werror")

#flags for specific build types
set(CMAKE_CXX_FLAGS_DEBUG "-g")

#check for the gcc version
if(CMAKE_COMPILER_IS_GNUCXX)
        execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE GCC_VERSION)
        # workaround for bug concerning -march=native switch in gcc 4.2.1 shipped with MacOS 10.7
        # see http://gcc.gnu.org/bugzilla/show_bug.cgi?id=33144
        if(GCC_VERSION VERSION_EQUAL "4.2.1")
                message(STATUS "Detected gcc version 4.2.1; -march=native switch is disabled because of gcc bug.")
                set(CMAKE_CXX_FLAGS_RELEASE "-O3")
        endif()
endif()

#some compiler flags
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-g ${CMAKE_CXX_FLAGS_RELEASE}")
set(CMAKE_CXX_LDFLAGS_DEBUG "-g")
set(CMAKE_CXX_LDFLAGS_RELEASE "-g -lgsl")
set(CMAKE_CXX_LDFLAGS_RELWITHDEBINFO "-g")

#some printouts
foreach(_LANG "C" "CXX")
        message(STATUS "Using ${_LANG} compiler '${CMAKE_${_LANG}_COMPILER}'.")
        message(STATUS "Using ${_LANG} compiler flags '${CMAKE_${_LANG}_FLAGS}'.")
endforeach()
unset(_LANG)

message(STATUS "Build type is '${CMAKE_BUILD_TYPE}'.")
message(STATUS "Using CXX compiler flags '${CMAKE_CXX_FLAGS_${CMAKE_BUILD_TYPE}}' "
        "for build type ${CMAKE_BUILD_TYPE}.")
message(STATUS "Using linker flags '${CMAKE_CXX_LDFLAGS_${CMAKE_BUILD_TYPE}}' "
        "for build type ${CMAKE_BUILD_TYPE}.")

      
#####
# set the build options
#####

# redirect the output files
message(STATUS "")
message(STATUS ">>> Setting up output paths.")

set(LIBRARY_OUTPUT_PATH "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")

message(STATUS "Using library output path '${LIBRARY_OUTPUT_PATH}'.")

set(EXECUTABLE_OUTPUT_PATH "${CMAKE_BINARY_DIR}/bin")

message(STATUS "Using executable output path '${EXECUTABLE_OUTPUT_PATH}'.")

# set include and library paths
set(UTILITIES_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/utilities")


#####
# setup Boost
#####

# environment variable $BOOST_ROOT is expected to point to non-standard locations
message(STATUS "")
message(STATUS ">>> Setting up Boost library.")
set(_BOOST_COMPONENTS "mpi" "serialization" "python" "timer" "system" "property_tree")

# set(Boost_DEBUG 1)
set(Boost_USE_STATIC_LIBS    OFF)
set(Boost_USE_MULTITHREADED  OFF)
set(Boost_USE_STATIC_RUNTIME OFF)

if(     ("$ENV{BOOST_ROOT}"       STREQUAL "")
                AND ("$ENV{BOOSTROOT}"        STREQUAL "")
                AND ("$ENV{Boost_DIR}"        STREQUAL "")
                AND ("$ENV{BOOST_INCLUDEDIR}" STREQUAL "")
                AND ("$ENV{BOOST_LIBRARYDIR}" STREQUAL ""))
        set(Boost_NO_SYSTEM_PATHS OFF)
else()
        set(Boost_NO_SYSTEM_PATHS ON)
endif()

# this is a somewhat ugly hack
# the problem is that components cannot be defined as optional while
# at the same time the library is required. the third find_package
# line is needed in case the components are not found, because
# Boost_FOUND is set to FALSE.
find_package(Boost 1.40.0 REQUIRED)
find_package(Boost 1.40.0 QUIET COMPONENTS ${_BOOST_COMPONENTS})
foreach(_BOOST_COMPONENT ${_BOOST_COMPONENTS})
        string(TOUPPER ${_BOOST_COMPONENT} _BOOST_COMPONENT)
        if(Boost_${_BOOST_COMPONENT}_FOUND)
                message(STATUS "    Found Boost component ${_BOOST_COMPONENT} at "
                        "'${Boost_${_BOOST_COMPONENT}_LIBRARY}'.")
        endif()
endforeach()
unset(_BOOST_COMPONENT)
unset(_BOOST_COMPONENTS)
find_package(Boost 1.40.0 REQUIRED)
set_feature_info(Boost
        "C++ Template Library"
        "http://www.boost.org/"
        "required"
        )
if(Boost_FOUND)
        set(Boost_LIBRARY_VERSION "${Boost_MAJOR_VERSION}.${Boost_MINOR_VERSION}.${Boost_SUBMINOR_VERSION}")
        message(STATUS "Using Boost ${Boost_LIBRARY_VERSION} include directories '${Boost_INCLUDE_DIRS}'.")
        message(STATUS "Using Boost ${Boost_LIBRARY_VERSION} library directories '${Boost_LIBRARY_DIRS}'.")
else()
        message(FATAL_ERROR "Could not find Boost installation. "
                "Is environment variable BOOST_ROOT=${BOOST_ROOT} set correctly? Please read INSTALL.")
endif()


#######
# setup the Doxygen documentation
#######

#add a target to generate API documentation with Doxygen
find_package(Doxygen)

if(DOXYGEN_FOUND)
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/include/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
  add_custom_target(doc
    ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Generating API documentation with Doxygen" VERBATIM
  )
endif(DOXYGEN_FOUND)

#######
# setup ROOT
#######

message(STATUS "")
message(STATUS ">>> Setting up ROOT")

#set the Root components that I need
set(_ROOT_COMPONENTS "MathCore" "Graf3d" "Eve")

#find ROOT
find_package(ROOT REQUIRED COMPONENTS ${_ROOT_COMPONENTS})

# to include the FindROOT.cmake file
#set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR} ${CMAKE_MODULE_PATH})

#add ROOT header paths
include_directories(${ROOT_INCLUDE_DIR})

#define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
include(${ROOT_USE_FILE})

#print the ROOT options
message(STATUS "Using ROOT ${ROOT_VERSION}.\nComponents '${_ROOT_COMPONENTS}'.\nInclude directories '${ROOT_INCLUDE_DIRS}'.\nLibraries '${ROOT_LIBRARIES}'.\nUse File '${ROOT_USE_FILE}'")
message(STATUS "Using ROOT cxx flags '${ROOT_CXX_FLAGS}'")

########
# finally build everything
########

#set the source and include directories
include_directories(include)

#generate a dictionary with rootcling, to handle custom classes
#https://root.cern.ch/how/integrate-root-my-project-cmake
#https://root.cern.ch/root/htmldoc/guides/users-guide/Cling.html
set(CMAKE_INSTALL_LIBDIR "${CMAKE_BINARY_DIR}/lib")
ROOT_GENERATE_DICTIONARY(G__DICT include/TFRParticle.h include/TFRTrack.h include/TFREvent.h include/TFRMCHit.h include/TFRCluster.h include/TFRLayer.h include/TFRState.h include/TFRFitnode.h MODULE DICT LINKDEF include/dict_LinkDef.h)

add_library(DICT SHARED include/TFRParticle.cpp include/TFRTrack.cpp include/TFRLayer.cpp include/TFRCluster.cpp G__DICT.cxx)
target_link_libraries(DICT ${ROOT_LIBRARIES})

#add the custom libraries
add_library(cparser include/cparser.cpp)
add_library(geometry include/TFRGeometry.cpp)
add_library(propagator include/TFRPropagator.cpp)
add_library(kalmanfilter include/TFRKalmanFilter)
add_library(chisqfit include/TFRChiSquaredFit.cpp)

#link the custom dictionary with the libraries that I need
target_link_libraries(DICT geometry)
target_link_libraries(DICT propagator)
target_link_libraries(DICT kalmanfilter)
target_link_libraries(DICT chisqfit)

# build all the executables
message( STATUS "Bin files will be written to: " ${EXECUTABLE_OUTPUT_PATH} )
file( GLOB_RECURSE APP_SOURCES src/*.cpp )

foreach( appsourcefile ${APP_SOURCES} )

    # get the filename and cut off the .cpp)
    get_filename_component(testname ${appsourcefile} NAME)
    string( REPLACE ".cpp" "" executablename ${testname} )
 
    # create the executable
    add_executable( ${executablename} ${appsourcefile} ${ROOT_HEADERS} )

    # link all the libraries
    target_link_libraries( ${executablename} cparser geometry propagator chisqfit kalmanfilter DICT ${ROOT_LIBRARIES} )

endforeach( appsourcefile ${APP_SOURCES} )

