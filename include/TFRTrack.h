#ifndef INCLUDE_TFRTRACK_H
#define INCLUDE_TFRTRACK_H 1

/*!
 *  @author    Alessio Piucci, 
 *  @brief     Representation of reconstructed tracks
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//ROOT libraries
#include "TROOT.h"
#include "TEveTrack.h"
#include "TEveTrackPropagator.h"
#include "TVector3.h"

//custom libraries
#include "TFRState.h"
#include "TFRFitnode.h"
#include "TFRParticle.h"

using namespace std;

class TFRTrack : public TEveRecTrackD {
 public:

  /*! Empty constructor */
  TFRTrack(){
    initial_state = new TFRState();
    momentum = TVector3(0., 0., 999999.);
    ax = 0;
    bx = 0;
    cx = 0;
    ay = 0;
    by = 0;
    cy = 0;
    charge = 1;
    fit_status = false;
    clusters = new TFRClusters();
    states_km = new TFRStates();
    initial_state = new TFRState();
    fitnodes = new TFRFitnodes();
    particles = new TFRParticles();
    covariance_matrix.ResizeTo(6, 6);
    covariance_matrix = TMatrixD(6, 6);  //up to quadratic parametrization
  };

  /*! Constructor with clusters */
  TFRTrack(TFRClusters *_clusters);
  
  /*! Destructor */
  virtual ~TFRTrack( ){ };

  /*! Add the particle to the vector of particles */  
  void AddParticle(TFRParticle *particle);

  /*! Get the particles associated to the track */
  inline TFRParticles* GetParticles(){return particles;};
  
  /*! Add the cluster to the vector of clusters */
  void AddCluster(TFRCluster *cluster);

  /*! Set the initial state of the track */
  inline void SetInitialState(TFRState *_initial_state){
    initial_state = _initial_state;};

  /*! Add the state to the vector of states used by the Kalman Filter */
  void AddState(TFRState *state);   
  
  /*! Set the clusters of the track */ 
  inline void SetClusters(TFRClusters* _clusters){clusters = _clusters;};
  
  /*! Get the clusters of the track */
  inline TFRClusters* GetClusters(){return clusters;};

  /*! Get the number of clusters on the track */
  inline unsigned int GetNClusters() const {return clusters->GetEntries();};
  
  /*! Get the states of the track used by the Kalman FIlter */
  inline TFRStates* GetStates(){return states_km;};
  
  /*! Get the kalman filtered states of the track that is closest to z*/
  TFRState* GetStateAt(double z);

  /*! Returns the inital state of the track*/
  inline TFRState* GetInitialState(){return initial_state;};
  
  /*! Get the kalman filtered state at z = 0*/
  inline TFRState* GetKalmanFilteredStateAt0(){return GetStateAt(0);};
  
  /*! Set the track chi2 */
  inline void SetChi2(const double _chi2, const bool forKalman = false){
    if(!forKalman) chi2 = _chi2;
    else chi2_km = _chi2;};
  
  /*! Set the track chi2 normalised by the number of degrees of freedom */
  inline void SetChi2NdoF(const double _chi2Ndof, const bool forKalman = false){
    if(!forKalman) chi2_NdoF = _chi2Ndof;
    else chi2_km_NdoF = _chi2Ndof;};
  
  /*! Set the number of degrees of freedom */
  inline void SetNdoF(const int _ndof, const bool forKalman = false){
    if(!forKalman) NdoF = _ndof;
    else NdoF_km = _ndof;};
  
  /*! Returns the track chi2 */
  inline double GetChi2(const bool forKalman = false) const {
    if(!forKalman) return chi2;
    else return chi2_km;};

  /*! Returns the track chi2 normalised by the number of degrees of freedom */
  inline double GetChi2NdoF(const bool forKalman = false) const {
    if(!forKalman) return chi2_NdoF;
    else return chi2_km_NdoF;};
  
  /*! Set the parameters of the track on the xz view */
  inline void SetTrackParamsXZ(const double _ax, const double _bx, const double _cx = 0.){
    ax = _ax; bx = _bx; cx = _cx;};

  /*! Set the parameters of the track on the xz view */
  inline void SetTrackParamsXZ(const TVector3 params_xz){
    ax = params_xz(0); bx = params_xz(1); cx = params_xz(2);};
  
  /*! Set the parameters of the track on the yz view */
  inline void SetTrackParamsYZ(const double _ay, const double _by, const double _cy = 0.){
    ay = _ay; by = _by; cy = _cy;};

  /*! Set the parameters of the track on the yz view */
  inline void SetTrackParamsYZ(const TVector3 params_yz){
    ay = params_yz(0); by = params_yz(1); cy = params_yz(2);};
  
  /*! Returns the number of degrees of freedom */
  inline int GetNdoF(const bool forKalman = false) const {
    if(!forKalman) return NdoF;
    else return NdoF_km;};
  
  /*! Returns the parameters of the track on the xz view */
  inline TVector3 GetTrackParamsXZ() const {
    return TVector3(ax, bx, cx);
  };

  /*! Returns the parameters of the track on the yz view */
  inline TVector3 GetTrackParamsYZ() const {
    return TVector3(ay, by, cy);
  };
  
  /*! Returns the slope in x of the track */
  inline double GetSlopeX() const {return bx;};

  /*! Returns the slope in y of the track */
  inline double GetSlopeY() const {return by;};
  
  /*! Create fitnodes from clusters*/
  void CreateFitnodes();

  /*! Get fitnodes*/
  TFRFitnodes* GetFitnodes( ){ return fitnodes;};

  /*! Get the momentum */
  inline TVector3 GetMomentum(){ return momentum;};
  
  /*! Get the momentum modulus */
  inline double GetMomentumMod(){ return momentum.Mag();};

  /*! Get the momentum errors */
  inline TVector3 GetMomentumErr(){ return momentum_err;};
  
  /*! Get the momentum error modulus */
  inline double GetMomentumErrMod(){ return momentum_err.Mag();};
  
  /*! Get the charge estimate */
  inline double GetCharge(){ return charge; };
  
  /*! Set the momentum */
  inline void SetMomentum(const TVector3 _momentum){
    momentum = _momentum;};

  /*! Set the momentum errors */
  inline void SetMomentumErr(const TVector3 _momentum_err){
    momentum_err = _momentum_err;};
  
  /*! Set the fitting status */
  inline void SetFitStatus(const bool status){ fit_status = status;};
  
  /*! Returns the fitting status */
  inline bool GetFitStatus() const { return fit_status;};

  /*! Set the full covariance matrix */
  inline void SetCovarianceMatrix(TMatrixD _covariance_matrix) {
    covariance_matrix = _covariance_matrix;};

  /*! Set the full covariance sub-matrix for the xz view */
  inline void SetCovarianceMatrixXZ(const TMatrixD _covariance_matrix) {
    covariance_matrix.SetSub(0, 0, _covariance_matrix);};

  /*! Set the full covariance sub-matrix for the yz view */
  inline void SetCovarianceMatrixYZ(const TMatrixD _covariance_matrix) {
    covariance_matrix.SetSub(3, 3, _covariance_matrix);};
  
  /*! Returns the full covariance matrix */
  inline TMatrixD GetCovarianceMatrix() const { return covariance_matrix;};

  /*! Returns the covariance sub-matrix for the xz track view */
  inline TMatrixD GetCovarianceMatrixXZ() const {
    return covariance_matrix.GetSub(0, 2, 0, 2);
  };

  /*! Returns the covariance sub-matrix for the yz track view */
  inline TMatrixD GetCovarianceMatrixYZ() const {
    return covariance_matrix.GetSub(3, 5, 3, 5);
  };

  /*! Returns the errors of the track parameters on the xz view */
  inline TVector3 GetErrorParamsXZ() const {
    return TVector3(covariance_matrix(0, 0),
		    covariance_matrix(1, 1),
		    covariance_matrix(2, 2));
  };
  
  /* Set charge estimate */
  void SetCharge(const double ch){ charge=ch; };
  
 protected:
  
 private:

  /*! Clusters of the track */
  TFRClusters *clusters;

  /*! Initial state of the track */
  TFRState *initial_state;
  
  /*! States of the track, used by the Kalman Filter */ 
  TFRStates *states_km;

  /*! Fitnodes of the track */ 
  TFRFitnodes *fitnodes;
  
  /*! Particles associated to the track*/
  TFRParticles *particles;
  
  /*! Momentum estimate of the track at the PV*/
  TVector3 momentum;

  /*! Error on the momentum estimate */
  TVector3 momentum_err;
  
  /*! Charge estimate of the track*/
  double charge;

  /*! Parameters of the track on the xz view, up to the quadratic representation (y = a + b*x + c*x^2) */
  double ax, bx, cx;

  /*! Parameters of the track on the yz view, up to the quadratic representation (y = a + b*x + c*x^2) */
  double ay, by, cy;
  
  /*! Full covariance matrix */
  TMatrixD covariance_matrix;
  
  /*! Track chi2 */
  double chi2;
  
  /*! Track chi2 from Kalman Filter*/
  double chi2_km;

  /*! Track chi2 normalised by the number of degrees of freedom */
  double chi2_NdoF;

  /*! Track chi2 from Kalman Filter normalised by the number of degrees of freedom */
  double chi2_km_NdoF;

  /*! Number of degrees of freedom in chi2 fit */
  int NdoF;

  /*! Number of degrees of freedom in Kalman fit */
  int NdoF_km;

  /*! Fitting status of track: true = everything OK; false = you should be worried about it... */ 
  bool fit_status;
  
  //magic happens here
  ClassDef(TFRTrack, 1)

};

/*! Array of tracks */
typedef TObjArray TFRTracks;

#endif // INCLUDE_TFRTRACK_H
