#ifndef INCLUDE_TFRCHISQUAREDFIT_H
#define INCLUDE_TFRCHISQUAREDFIT_H 1

/*!
 *  @author    Alessio Piucci
 *  @brief     Library to implement chi2 fit
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//ROOt libraries
#include "TROOT.h"

//custom libraries
#include "TFRGeometry.h"
#include "TFRCluster.h"
#include "TFRTrack.h"

using namespace std;

class TFRChiSquaredFit {

 public:
  
  /*! Standard constructor */
  TFRChiSquaredFit(TFRGeometry *_geometry) :
   geometry(_geometry) {};
  
  /*! Destructor */
  virtual ~TFRChiSquaredFit( ){ };

  /*! Perform a 1D linear fit */
  bool FitLinear1D(TFRTrack *track, bool xz_view = true);

  /*! Perform a 1D linear fit, with matrix formalism */
  bool FitLinear1D_matrix(TFRTrack *track, bool xz_view,
			  TFRGeometry *detector_geo);
  
  /*! Perform a 1D quadratic fit, with matrix formalism */
  bool FitQuadratic1D_matrix(TFRTrack *track, bool xz_view,
			     TFRGeometry *detector_geo);

  /*! Perform a 2D linear fit, with matrix formalism */
  bool FitLinear2D_matrix(TFRTrack *track, TFRGeometry *detector_geo);
  
 protected:

 private:

  /*! Detector geometry */
  TFRGeometry *geometry;
  
};

#endif // INCLUDE_TFRCHISQUAREDFIT_H
